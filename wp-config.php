<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_charity');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`wgvVub>1H=H#4{nR(Djaxr YeJGzM<#heKTYE3ERKN~<B780Ev/]1i2dX3U&UK:');
define('SECURE_AUTH_KEY',  'BUe|$#IcE~Ku~$GS_e>=<CMx;#Dgu#D2.6jM=^t2+MI?xk6?>#[YGiK;D@P18wks');
define('LOGGED_IN_KEY',    'WlrXMNKg$b=a+;UiPwR;l!|KaJB.Xjlq#nB(H-m1q,ktU=>(LweZ,i<T<<J~SWuz');
define('NONCE_KEY',        '+q({,fA`=uV2tWKm>,FaPHqz3l{K>w_=MI~-*~fk+3_6k_GrHx(Fj/w[I#wc!Zt3');
define('AUTH_SALT',        'lJ?}`,KPbFZBe(!Nj:CDI~C+vnU/VlnWg22[3c]et6?A<5Js%E8 B;!>jYhg:7Ne');
define('SECURE_AUTH_SALT', '}W#mvrNo*t[]`HmXp^o`eF-/8MI_*YYCK&azZdp9-7gMGz>qn8PYz5nlM.z(21pI');
define('LOGGED_IN_SALT',   '-k2o+hX2%s [[_TY8<=_9JhAoF)uo.E2>j5869 k:s?oBs?7)GXawE73t!X/loZy');
define('NONCE_SALT',       '%{v`,sijJ_60qdxRjAo(5GC#T+`pNB(>>O+X.?:8_!c<_3;U;?mN_:nx?EYS)VV&');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
